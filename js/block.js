/**
 * @file
 * Provides UI/UX progressive enhancements on Olivero's theme settings by
 * creating an HTMLColorInput element and synchronizing its input with a text
 * input to provide an accessible and user-friendly interface. Additionally,
 * provides a select element with pre-defined color values for easy color
 * switching.
 */

((Drupal, settings, once) => {
  /**
   * Initializes DubBot block dynamic styles.
   *
   * @param {HTMLElement} block The DubBot block element
   */
  function initBlockColor(block) {
    const linkColor = block.getAttribute('data-drupal-dubbot-color');

    if (linkColor.length !== 7) {
      return;
    }

    // Create CSS element.
    const blockStyles = document.createElement('style');
    const selector = block.getAttribute('data-drupal-dubbot-selector');

    let html = `${selector} {color: ${linkColor};}`;
    html += `${selector}:before {background-image: url("/dubbot-icon/${linkColor.substring(1)}");}`;

    blockStyles.innerHTML = html;
    document.head.appendChild(blockStyles);
  }

  /**
   * DubBot block behavior.
   *
   * @type {Drupal~behavior}
   * @prop {Drupal~behaviorAttach} attach
   *   Initializes color picker fields.
   */
  Drupal.behaviors.dubbotBlock = {
    attach: () => {
      const dubbotBlocks = once(
        'dubbot-report-block',
        'div.dubbot-report-block',
      );

      dubbotBlocks.forEach((block) => {
        initBlockColor(block);
        const dubbotApiUrl = settings.dubbot.block.api_url;
        const api = new DubbotHighlight('#page', dubbotApiUrl);
        api.init();
      });


    },
  };
})(Drupal, drupalSettings, once);
