/**
 * @file
 * Provides UI/UX progressive enhancements on DubBot's block settings by
 * creating an HTMLColorInput element and synchronizing its input with a text
 * input to provide an accessible and user-friendly interface.
 */

((Drupal, settings, once) => {
  /**
   * `input` event callback to keep text & color inputs in sync.
   *
   * @param {HTMLElement} changedInput input element changed by user
   * @param {HTMLElement} inputToSync input element to synchronize
   */
  function synchronizeInputs(changedInput, inputToSync) {
    inputToSync.value = changedInput.value;

    changedInput.setAttribute('data-dubbot-custom-color', changedInput.value);
    inputToSync.setAttribute('data-dubbot-custom-color', changedInput.value);
  }

  /**
   * Initializes DubBot theme-settings color picker.
   *   creates a color-type input and inserts it after the original text field.
   *   modifies aria values to make label apply to both inputs.
   *   adds event listeners to keep text & color inputs in sync.
   *
   * @param {HTMLElement} textInput The textfield input from the Drupal form API
   */
  function initColorPicker(textInput) {
    // Create input element.
    const colorInput = document.createElement('input');

    // Set new input's attributes.
    colorInput.type = 'color';
    colorInput.classList.add(
      'form-color',
      'form-element',
      'form-element--type-color',
      'form-element--api-color',
    );
    colorInput.value = textInput.value;
    colorInput.setAttribute('name', `${textInput.name}_visual`);
    colorInput.setAttribute(
      'data-dubbot-custom-color',
      textInput.getAttribute('data-dubbot-custom-color'),
    );

    // Insert new input into DOM.
    textInput.after(colorInput);

    // Make field label apply to textInput and colorInput.
    const fieldID = textInput.id;
    const label = document.querySelector(`label[for="${fieldID}"]`);
    label.removeAttribute('for');
    label.setAttribute('id', `${fieldID}-label`);

    textInput.setAttribute('aria-labelledby', `${fieldID}-label`);
    colorInput.setAttribute('aria-labelledby', `${fieldID}-label`);

    // Add `input` event listener to keep inputs synchronized.
    textInput.addEventListener('input', () => {
      synchronizeInputs(textInput, colorInput);
    });

    colorInput.addEventListener('input', () => {
      synchronizeInputs(colorInput, textInput);
    });
  }

  /**
   * DubBot Color Picker behavior.
   *
   * @type {Drupal~behavior}
   * @prop {Drupal~behaviorAttach} attach
   *   Initializes color picker fields.
   */
  Drupal.behaviors.dubbotColorPicker = {
    attach: () => {
      const colorTextInputs = once(
        'dubbot-color-picker',
        '[data-drupal-selector="dubbot-color-picker"] input[type="text"]',
      );

      colorTextInputs.forEach((textInput) => {
        initColorPicker(textInput);
      });
    },
  };
})(Drupal, drupalSettings, once);
