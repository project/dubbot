<?php

namespace Drupal\dubbot_toolbar;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\dubbot\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handler class to manage DubBot toolbar integration.
 */
class ToolbarHandler implements ContainerInjectionInterface {

  /**
   * The access manager service.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected $accessManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The DubBot link generator.
   *
   * @var \Drupal\dubbot\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * Constructs a new ToolbarHandler instance.
   *
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The access manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\dubbot\LinkGeneratorInterface $link_generator
   *   The DubBot link generator.
   */
  public function __construct(AccessManagerInterface $access_manager, RouteMatchInterface $route_match, LinkGeneratorInterface $link_generator) {
    $this->accessManager = $access_manager;
    $this->routeMatch = $route_match;
    $this->linkGenerator = $link_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('access_manager'),
      $container->get('current_route_match'),
      $container->get('dubbot.link_generator')
    );
  }

  /**
   * Hook bridge.
   *
   * @return array
   *   The dubbot toolbar items render array.
   *
   * @see dubbot_toolbar()
   */
  public function toolbar(): array {
    $items['dubbot'] = [
      '#cache' => [
        'contexts' => ['user.permissions'],
        'tags' => [LinkGeneratorInterface::LINK_CACHE_TAG],
      ],
    ];

    if (!$this->accessManager->checkNamedRoute('dubbot.report')) {
      return $items;
    }
    $items['dubbot']['#cache']['contexts'][] = 'url';

    // Hide the toolbar item if the page is not accessible for anonymous users,
    // hence cannot be crawled.
    if (in_array($this->routeMatch->getRouteName(), ['system.404', 'system.403']) || !$this->accessManager->check($this->routeMatch, new AnonymousUserSession())) {
      return $items;
    }

    $link = $this->linkGenerator->generate(['toolbar-icon']);

    if (!$link) {
      return $items;
    }

    $items['dubbot'] += [
      '#type' => 'toolbar_item',
      '#weight' => 125,
      'tab' => $link->toRenderable(),
      '#attached' => [
        'library' => ['dubbot_toolbar/toolbar'],
      ],
    ];

    return $items;
  }

}
