<?php

namespace Drupal\Tests\dubbot_toolbar\Unit;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\dubbot_toolbar\ToolbarHandler;
use Drupal\Tests\UnitTestCase;

/**
 * @covers \Drupal\dubbot_toolbar\ToolbarHandler
 * @group dubbot
 */
class ToolbarHandlerTest extends UnitTestCase {

  /**
   * The access Manager service.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected $accessManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The DubBot link generator.
   *
   * @var \Drupal\dubbot\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * The Toolbar Handler instance.
   *
   * @var \Drupal\dubbot_toolbar\ToolbarHandler
   */
  protected $toolbarHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->accessManager = $this->createMock('Drupal\Core\Access\AccessManagerInterface');
    $this->routeMatch = $this->createMock('Drupal\Core\Routing\RouteMatchInterface');
    $this->linkGenerator = $this->createMock('Drupal\dubbot\LinkGenerator');

    $this->routeMatch->expects($this->any())
      ->method('getRouteName')
      ->willReturn('system');

    $this->linkGenerator->expects($this->any())
      ->method('generate')
      ->willReturn(Link::fromTextAndUrl('DubBot', new Url('<front>')));

    $this->toolbarHandler = new ToolbarHandler($this->accessManager, $this->routeMatch, $this->linkGenerator);
  }

  /**
   * Tests access to the toolbar item.
   */
  public function testToolbarItemAccess(): void {
    $this->accessManager->expects($this->any())
      ->method('checkNamedRoute')
      ->willReturn(FALSE, TRUE, TRUE);
    $this->accessManager->expects($this->any())
      ->method('check')
      ->willReturn(FALSE, TRUE);

    // User has no access to DubBot reports.
    $toolbar_item = $this->toolbarHandler->toolbar();
    $this->assertArrayNotHasKey('tab', $toolbar_item['dubbot']);
    $this->assertNotContains('url', $toolbar_item['dubbot']['#cache']['contexts']);

    // User has access to DubBot reports, but the page is not public.
    $toolbar_item = $this->toolbarHandler->toolbar();
    $this->assertArrayNotHasKey('tab', $toolbar_item['dubbot']);
    $this->assertContains('url', $toolbar_item['dubbot']['#cache']['contexts']);

    // User has access to DubBot reports, and the page is public.
    $toolbar_item = $this->toolbarHandler->toolbar();
    $this->assertArrayHasKey('tab', $toolbar_item['dubbot']);
    $this->assertContains('url', $toolbar_item['dubbot']['#cache']['contexts']);
  }

  /**
   * Tests the toolbar item markup.
   */
  public function testToolbarItemMarkup(): void {
    $this->accessManager->expects($this->any())
      ->method('checkNamedRoute')
      ->willReturn(TRUE);
    $this->accessManager->expects($this->any())
      ->method('check')
      ->willReturn(TRUE);

    $toolbar_item = $this->toolbarHandler->toolbar();
    $this->assertArrayHasKey('tab', $toolbar_item['dubbot']);
    $this->assertEquals('DubBot', $toolbar_item['dubbot']['tab']['#title']);
    $this->assertEquals('<front>', $toolbar_item['dubbot']['tab']['#url']->getRouteName());
    $this->assertArrayHasKey('#attached', $toolbar_item['dubbot']);
    $this->assertEquals([
      'library' => ['dubbot_toolbar/toolbar'],
    ], $toolbar_item['dubbot']['#attached']);
  }

}
