# DubBot Module Changelog
## 2.0.0 Release

- Issue [#3489236](https://dgo.to/3489236) 2.0.0 Release plan.
- Issue [#3484445](https://dgo.to/3484445) Create Permissions to limit which reports appear in DubBot widget
- Issue [#3484440](https://dgo.to/3484440) Allow DubBot iframe to hightlight Drupal Markup


## 1.0.1 Release

- Issue [#3484998](https://dgo.to/3484998): 1.0.1 Release plan.
- Issue [#3429983](https://dgo.to/3429983): Automated Drupal 11 compatibility fixes for dubbot, part 2.
- Issue [#3396448](https://dgo.to/3396448): Make DUBBOT iframe bigger
- Issue [#3429983](https://dgo.to/3429983): Automated Project Update Bot fixes

## 1.0.0 Release

Previous release commits not listed.
