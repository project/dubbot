<?php

/**
 * @file
 * Hooks for the DubBot module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Perform alterations to the domains provided by Domain Negotiator service.
 *
 * Domains to be managed by DubBot are determined by Domain Negotiator service.
 * This hook allows modules to alter the list of domains.
 *
 * @param array $domains
 *   Array of domains to be included in DubBot reports.
 *
 * @see \Drupal\dubbot\DomainNegotiator
 */
function hook_dubbot_domains_alter(array &$domains) {
  // Add a new domain.
  $domains[] = 'https://foo.com';
  // Remove a domain.
  $key = array_search('https://bar.com', $domains);
  if ($key !== FALSE) {
    unset($domains[$key]);
  }
}

/**
 * @} End of "addtogroup hooks".
 */
