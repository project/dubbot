<?php

namespace Drupal\dubbot;

/**
 * Client interface for sending HTTP requests to DubBot.
 *
 * @internal
 */
interface ClientInterface {

  const API_BASE_URL = 'https://api.dubbot.com';
  const APP_BASE_URL = 'https://app.dubbot.com';

  const EMBED_ENDPOINT_PATH = '/embeds/:embed_key';
  const PAGES_ENDPOINT_PATH = '/embeds/:embed_key/pages';

  const DASHBOARD_PAGE_PATH = '/sites/:site_id/page/:page_id';

  const CLIENT_CACHE_TAG = 'dubbot:client';
  const CLIENT_CACHE_ENABLED_CID = 'dubbot_client_enabled';

  /**
   * Indicates whether the client and the DubBot integration is enabled.
   *
   * This is based on the entered key and whether is valid or not. Result is
   * cached, so it is not 100% accurate, but enough for module purposes. To
   * have a 100% accurate status of the integration, call directly to
   * ClientInterface::isValidEmbedKey().
   *
   * @return bool
   *   Bool indicating whether the client is enabled or not.
   */
  public function isEnabled(): bool;

  /**
   * Checks if a given string is a valid DubBot Embed Key.
   *
   * @param string $embed_key
   *   The embed key value to check.
   *
   * @return bool
   *   TRUE if the embed key is valid. FALSE otherwise.
   */
  public function isValidEmbedKey(string $embed_key): bool;

  /**
   * Returns the iframe URL for a given URL.
   *
   * @param string $url
   *   The URL the report relates to.
   *
   * @return string|null
   *   The iframe Report URL for the given URL. NULL if there is no embed key.
   */
  public function iframeReportUrlByUrl(string $url): ?string;

  /**
   * Returns the iframe URL for a given page ID.
   *
   * @param string $page_id
   *   The page ID the report relates to.
   *
   * @return string|null
   *   The iframe Report URL for the given URL. NULL if there is no embed key.
   */
  public function iframeReportUrlByPageId(string $page_id): ?string;

  /**
   * Returns the report for a given URL.
   *
   * @param string $url
   *   The URL the report relates to.
   *
   * @return \Drupal\dubbot\DubBotEmbedJsonResponse
   *   The report response.
   */
  public function reportByUrl(string $url): DubBotEmbedJsonResponse;

  /**
   * Returns the report for a given URL.
   *
   * @param string $page_id
   *   The URL the report relates to.
   *
   * @return \Drupal\dubbot\DubBotEmbedJsonResponse
   *   The report response.
   */
  public function reportByPageId(string $page_id): DubBotEmbedJsonResponse;

  /**
   * Returns the overview results for a given set of parameters.
   *
   * @param int $page
   *   The requested page.
   * @param int $limit
   *   The numbers of items to include in each page.
   * @param string $sort
   *   (Optional) The column to sort the results by. Defaults to "title".
   * @param string $order
   *   The sorting order "asc" or "desc". Defaults to "asc".
   *
   * @return object
   *   The overview page object.
   */
  public function overview(int $page, int $limit, string $sort = 'title', string $order = 'asc'): object;

}
