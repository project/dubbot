<?php

namespace Drupal\dubbot\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\dubbot\ClientInterface;
use Drupal\dubbot\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure DubBot settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The DubBot client service.
   *
   * @var \Drupal\dubbot\ClientInterface
   */
  protected $client;

  /**
   * {@inheritDoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, ClientInterface $client) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('dubbot.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'dubbot_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['dubbot.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['embed_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Embed key'),
      '#default_value' => $this->config('dubbot.settings')->get('embed_key'),
      '#description' => $this->t('Embed key is provided by DubBot support team. Please write your request to <a href=":mailto">help@dubbot.com</a>.', [':mailto' => Url::fromUri('mailto:help@dubbot.com')->toString()]),
    ];

    $form['api_url'] = [
      '#title' => $this->t('Dubbot API URL'),
      '#type' => 'textfield',
      '#default_value' => $this->config('dubbot.settings')->get('api_url'),
      '#description' => $this->t('Update only if directed to do so.'),
    ];

    $form['dialog_renderer'] = [
      '#title' => $this->t('DubBot Report position'),
      '#type' => 'radios',
      '#options' => [
        '0' => $this->t('Modal'),
        'off_canvas' => $this->t('Side'),
        'off_canvas_top' => $this->t('Top'),
      ],
      '#description' => $this->t('Select the position of the in-page DubBot report.'),
      '#default_value' => $this->config('dubbot.settings')->get('dialog_renderer'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    try {
      if (!$this->client->isValidEmbedKey($form_state->getValue('embed_key'))) {
        $form_state->setErrorByName('embed_key', $this->t('The embed key is not valid.'));
      }
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('embed_key', $this->t('Unable to validate the embed key. Please try again or contact the site administrator.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('dubbot.settings')
      ->set('embed_key', $form_state->getValue('embed_key'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('dialog_renderer', $form_state->getValue('dialog_renderer'))
      ->save();

    Cache::invalidateTags([
      LinkGeneratorInterface::LINK_CACHE_TAG,
      ClientInterface::CLIENT_CACHE_TAG,
    ]);

    parent::submitForm($form, $form_state);
  }

}
