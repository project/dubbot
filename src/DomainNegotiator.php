<?php

namespace Drupal\dubbot;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Negotiates the available domains to request to DubBot.
 */
class DomainNegotiator implements DomainNegotiatorInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a DomainNegotiator object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(RequestStack $request_stack, ModuleHandlerInterface $module_handler) {
    $this->requestStack = $request_stack;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function domains(): array {
    $domains = [];

    $request = $this->requestStack->getCurrentRequest();
    $domains[] = $request->getSchemeAndHttpHost();

    $this->moduleHandler->alter('dubbot_domains', $domains);

    return $domains;
  }

}
