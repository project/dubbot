<?php

namespace Drupal\dubbot\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\dubbot\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a DubBot report block.
 *
 * @Block(
 *   id = "dubbot_report",
 *   admin_label = @Translation("DubBot Report"),
 *   category = @Translation("DubBot")
 * )
 */
class DubBotReportBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The access manager service.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected $accessManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The DubBot link generator.
   *
   * @var \Drupal\dubbot\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * Dubbot Settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $dubbotSettings;

  /**
   * Constructs a new DubBotReportBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The access manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\dubbot\LinkGeneratorInterface $link_generator
   *   The DubBot link generator.
   * @param \Drupal\Core\Config\ImmutableConfig $dubbot_settings
   *   The Dubbot settings.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccessManagerInterface $access_manager, RouteMatchInterface $route_match, LinkGeneratorInterface $link_generator, ImmutableConfig $dubbot_settings) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->accessManager = $access_manager;
    $this->routeMatch = $route_match;
    $this->linkGenerator = $link_generator;
    $this->dubbotSettings = $dubbot_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('access_manager'),
      $container->get('current_route_match'),
      $container->get('dubbot.link_generator'),
      $container->get('config.factory')->get('dubbot.settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'link_color' => '#1b9ae4',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['link_color'] = [
      '#type' => 'textfield',
      '#maxlength' => 7,
      '#size' => 10,
      '#title' => $this->t('DubBot link color'),
      '#description' => $this->t('Enter color in full hexadecimal format (#abc123).'),
      '#default_value' => $this->configuration['link_color'],
      '#required' => TRUE,
      '#attributes' => [
        'pattern' => '^#[a-fA-F0-9]{6}',
      ],
      '#wrapper_attributes' => [
        'data-drupal-selector' => 'dubbot-color-picker',
      ],
    ];

    $form['#attached']['library'][] = 'dubbot/color-picker';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $color = $form_state->getValue('link_color');

    if (!preg_match('/^#[a-fA-F0-9]{6}/', $color)) {
      $form_state->setErrorByName('link_color', $this->t('Please enter a valid HEX color code.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['link_color'] = $values['link_color'];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account): AccessResultInterface {
    return $this->accessManager->checkNamedRoute('dubbot.report', [], NULL, TRUE)
      ->andIf(AccessResult::allowedIf(
        !in_array($this->routeMatch->getRouteName(), [
          'system.404',
          'system.403',
        ])
        && $this->accessManager->check($this->routeMatch, new AnonymousUserSession())));
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $link = $this->linkGenerator->generate();
    if (!$link) {
      return [];
    }

    $id = Html::getUniqueId('dubbot-block');
    return [
      'content' => $link->toRenderable(),
      '#attributes' => [
        'class' => ['dubbot-report-block', $id],
        'data-drupal-dubbot-color' => $this->configuration['link_color'],
        'data-drupal-dubbot-selector' => ".dubbot-report-block.$id .dubbot-report-link",
      ],
      '#attached' => [
        'library' => ['dubbot/block'],
        'drupalSettings' => [
          'dubbot' => [
            'block' => [
              'link_color' => $this->configuration['link_color'],
              'api_url' => $this->dubbotSettings->get('api_url'),
            ],
          ],
        ],
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    $cache_tags = parent::getCacheTags();
    $cache_tags[] = LinkGeneratorInterface::LINK_CACHE_TAG;
    return $cache_tags;
  }

}
