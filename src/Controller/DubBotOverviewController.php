<?php

namespace Drupal\dubbot\Controller;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Utility\TableSort;
use Drupal\dubbot\ClientInterface;
use Drupal\dubbot\DubBotPageJsonResponse;
use Drupal\dubbot\DubBotPagesJsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for DubBot routes.
 */
class DubBotOverviewController implements ContainerInjectionInterface {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * The DubBot client.
   *
   * @var \Drupal\dubbot\ClientInterface
   */
  protected $client;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The number of items to list per page.
   *
   * @var int
   */
  protected $limit = 50;

  /**
   * The result of the request to the external endpoint.
   *
   * @var \Drupal\dubbot\DubBotPagesJsonResponse
   */
  protected $result;

  /**
   * Array containing the pager metadata.
   *
   * @var array
   */
  protected $pager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\dubbot\ClientInterface $client
   *   The DubBot client service.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(ClientInterface $client, PagerManagerInterface $pager_manager, DateFormatterInterface $date_formatter) {
    $this->client = $client;
    $this->pagerManager = $pager_manager;
    $this->dateFormatter = $date_formatter;
    $this->initializePager();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('dubbot.client'),
      $container->get('pager.manager'),
      $container->get('date.formatter'),
    );
  }

  /**
   * Builds the response.
   */
  public function build(): array {
    // Do not show the table if the DubBot service is unreachable.
    if (!$this->client->isEnabled()) {
      $this->messenger()->addError($this->t('DubBot Embed key is not set or is not valid. Please review it in the <a href=":url">Settings page</a>.', [':url' => Url::fromRoute('dubbot.settings')->toString()]));
      return [];
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#rows' => [],
      '#empty' => $this->t('There are no pages crawled by DubBot yet.'),
      '#cache' => [
        'tags' => [ClientInterface::CLIENT_CACHE_TAG],
      ],
    ];
    foreach ($this->pages() as $page) {
      if ($row = $this->buildRow($page)) {
        $build['table']['#rows'][$page->id()] = $row;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $build;
  }

  /**
   * Builds the header row for the entity listing.
   *
   * @return array
   *   A render array structure of header strings.
   *
   * @see \Drupal\Core\Entity\EntityListBuilder::render()
   */
  protected function buildHeader(): array {
    return [
      'page' => [
        'data' => $this->t('Page'),
        'field' => 'title',
        'specifier' => 'page',
        'sort' => 'asc',
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'issues_count' => [
        'data' => $this->t('Issues Count'),
        'field' => 'total_issues_count',
        'specifier' => 'total_issues_count',
        'initial_click_sort' => 'desc',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'crawled_at' => [
        'data' => $this->t('Crawled At'),
        'field' => 'crawled_at',
        'specifier' => 'crawled_at',
        'initial_click_sort' => 'desc',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'operations' => $this->t('Operations'),
    ];
  }

  /**
   * Performs the query against the DubBot server to retrieve the items.
   *
   * @return object
   *   The object returned by the DubBot endpoint.
   */
  protected function query(): object {
    if (isset($this->result)) {
      return $this->result;
    }

    $headers = $this->buildHeader();

    $order = TableSort::getOrder($headers, \Drupal::request());
    $direction = TableSort::getSort($headers, \Drupal::request());

    try {
      $this->result = $this->client->overview($this->pager['page'], $this->limit, $order['sql'], $direction);
      $total = $this->result->totalItems();
      $this->pagerManager->createPager($total, $this->limit, $this->pager['element']);
    }
    catch (\Exception $e) {
      $this->result = new DubBotPagesJsonResponse();
    }

    return $this->result;
  }

  /**
   * Retrieves the pages to include in the overview page from DubBot.
   *
   * @return array
   *   The pages to include in the overview page.
   */
  protected function pages(): array {
    return $this->query()->pages();
  }

  /**
   * Builds a row for an DubBot page in the listing.
   *
   * @param \Drupal\dubbot\DubBotPageJsonResponse $page
   *   The object for this row of the list.
   *
   * @return array
   *   A render array structure of fields for this entity.
   */
  protected function buildRow(DubBotPageJsonResponse $page): array {
    return [
      'page' => $page->link(),
      'issues_count' => $page->issuesCount(),
      'crawled_at' => $this->dateFormatter->format($page->crawledAt()->getTimestamp(), 'medium'),
      'operations' => [
        'data' => $this->buildOperations($page),
      ],
    ];
  }

  /**
   * Gets the total number of results and initialize a pager for the query.
   *
   * The pager can be disabled by either setting the pager limit to 0, or by
   * setting this query to be a count query.
   */
  protected function initializePager() {
    $element = $this->pagerManager->getMaxPagerElementId() + 1;

    $this->pager = [
      'limit' => $this->limit,
      'element' => $element,
      'page' => $this->pagerManager->findPage($element),
    ];
  }

  /**
   * Builds a renderable list of operation links for the row.
   *
   * @param \Drupal\dubbot\DubBotPageJsonResponse $row
   *   The object on which the linked operations will be performed.
   *
   * @return array
   *   A renderable array of operation links.
   *
   * @see \Drupal\Core\Entity\EntityListBuilder::buildRow()
   */
  protected function buildOperations(DubBotPageJsonResponse $row): array {
    $dubbot_report_uri = ClientInterface::APP_BASE_URL . strtr(ClientInterface::DASHBOARD_PAGE_PATH, [
      ':site_id' => $row->siteId(),
      ':page_id' => $row->id(),
    ]);
    return [
      '#type' => 'operations',
      '#links' => [
        'report' => [
          'title' => $this->t('View Report'),
          'weight' => 10,
          'url' => Url::fromRoute('dubbot.report', ['page_id' => $row->id()]),
        ],
        'dubbot_report' => [
          'title' => $this->t('View Report in DubBot'),
          'weight' => 20,
          'url' => Url::fromUri($dubbot_report_uri, [
            'attributes' => [
              'target' => '_blank',
            ],
          ]),
        ],
      ],
    ];
  }

}
