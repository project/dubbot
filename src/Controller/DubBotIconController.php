<?php

declare(strict_types=1);

namespace Drupal\dubbot\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Customizable DubBot icon controller.
 */
class DubBotIconController implements ContainerInjectionInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Custom access callback to the DubBot icon.
   *
   * @param string $color_code
   *   The icon's color HEX code.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(string $color_code = ''): AccessResultInterface {
    $matches = [];
    preg_match("/^(?>[[:xdigit:]]{3}){1,2}$/", $color_code, $matches);

    return AccessResult::allowedIf(!empty($matches));
  }

  /**
   * Builds the response.
   *
   * @param string $color_code
   *   The icon's color HEX code.
   *
   * @return \Drupal\Core\Cache\CacheableResponseInterface
   *   The icon response.
   */
  public function build(string $color_code = ''): CacheableResponseInterface {
    // Set up an empty response.
    $response = new CacheableResponse('', 200);
    $build['content'] = [
      '#theme' => 'dubbot_svg_icon',
      '#color' => '#' . $color_code,
    ];

    $output = (string) $this->renderer->renderRoot($build);

    if (empty($output)) {
      throw new NotFoundHttpException();
    }

    $response->setContent($output);
    $response->headers->set('Content-Type', 'image/svg+xml; charset=utf-8');
    $cache_metadata = CacheableMetadata::createFromRenderArray($build);
    $response->addCacheableDependency($cache_metadata);

    return $response;
  }

}
