<?php

namespace Drupal\dubbot\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\dubbot\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for DubBot routes.
 */
class DubBotReportController extends ControllerBase {

  /**
   * The DubBot client service.
   *
   * @var \Drupal\dubbot\ClientInterface
   */
  protected $client;

  /**
   * The controller constructor.
   *
   * @param \Drupal\dubbot\ClientInterface $client
   *   The dubbot.client service.
   */
  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('dubbot.client')
    );
  }

  /**
   * Custom access callback to the DubBot report page.
   *
   * @param string $page_id
   *   The page ID the report relates to.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(string $page_id = ''): AccessResultInterface {
    if (!$this->client->isEnabled()) {
      return AccessResult::forbidden('DubBot integration is not enabled')->addCacheTags([ClientInterface::CLIENT_CACHE_TAG]);
    }

    $access = $this->currentUser()->hasPermission('access dubbot report');
    if (!$access) {
      return AccessResult::forbidden("The 'access dubbot report' permission is required.")->addCacheContexts(['user.permissions']);
    }

    try {
      $response = $this->client->reportByPageId($page_id);
      return AccessResult::allowedIf($response->isValidResponse())->addCacheContexts(['user.permissions']);
    }
    catch (\Exception $e) {
      return AccessResult::forbidden('Unable to reach DubBot server')->setCacheMaxAge(60);
    }
  }

  /**
   * Builds the response.
   *
   * @param string $page_id
   *   The page ID the report relates to.
   *
   * @return array
   *   The controller output render array.
   */
  public function build(string $page_id = ''): array {
    $build['content'] = [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'src' => $this->client->iframeReportUrlByPageId($page_id),
        'allow' => 'fullscreen',
        'class' => ['dubbot-iframe'],
      ],
      '#attached' => [
        'library' => ['dubbot/iframe'],
      ],
    ];

    return $build;
  }

}
