<?php

namespace Drupal\dubbot;

/**
 * Negotiates the available domains to request to DubBot.
 */
interface DomainNegotiatorInterface {

  /**
   * Provides the available domains for DubBor.
   *
   * @return array
   *   The list of domains DubBot should check to build the reports.
   */
  public function domains(): array;

}
