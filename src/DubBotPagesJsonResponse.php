<?php

namespace Drupal\dubbot;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Psr7\Response;

/**
 * Maps the DubBot Pages JSON response.
 */
final class DubBotPagesJsonResponse {

  /**
   * The pages array.
   *
   * @var \Drupal\dubbot\DubBotPageJsonResponse[]
   */
  private $pages;

  /**
   * The current page index.
   *
   * @var int
   */
  private $currentPage;

  /**
   * The total number of pages.
   *
   * @var int
   */
  private $totalPages;

  /**
   * The total number of items.
   *
   * @var int
   */
  private $totalItems;

  /**
   * Constructs a new DubBotPagesJsonResponse instance.
   *
   * @param array $pages
   *   (Optional) The pages array. Defaults to [].
   * @param int $current_page
   *   (Optional) The current page index. Defaults to 0.
   * @param int $total_pages
   *   (Optional) The total number of pages. Defaults to 0.
   * @param int $total_items
   *   (Optional) The total number of items. Defaults to 0.
   */
  public function __construct(array $pages = [], int $current_page = 0, int $total_pages = 0, int $total_items = 0) {
    $this->pages = [];
    array_walk($pages, function (DubBotPageJsonResponse $page) {
      $this->pages[] = $page;
    });
    $this->currentPage = $current_page;
    $this->totalPages = $total_pages;
    $this->totalItems = $total_items;
  }

  /**
   * Builds a new DubBotPagesJsonResponse from a HTTP client response object.
   *
   * @param \GuzzleHttp\Psr7\Response $response
   *   The HTTP response.
   *
   * @return self
   *   The DubBotEmbedJsonResponse instance.
   */
  public static function fromHttpClientResponse(Response $response): self {
    $body = Json::decode($response->getBody());

    assert(is_array($body) && is_array($body['pages']) && is_array($body['pagination']));

    $pages = [];
    array_walk($body['pages'], function (array $page) use (&$pages) {
      $pages[] = DubBotPageJsonResponse::fromArray($page);
    });

    return new static(
      $pages,
      $body['pagination']['current_page'],
      $body['pagination']['total_pages'],
      $body['pagination']['total_items']
    );
  }

  /**
   * Restunrs the response pages array.
   *
   * @return array|\Drupal\dubbot\DubBotPageJsonResponse[]
   *   The response pages array.
   */
  public function pages(): array {
    return $this->pages;
  }

  /**
   * Returns the current page.
   *
   * @return int
   *   The current page.
   */
  public function currentPage(): int {
    return $this->currentPage;
  }

  /**
   * Returns the total pages.
   *
   * @return int
   *   The total pages.
   */
  public function totalPages(): int {
    return $this->totalPages;
  }

  /**
   * Returns the total items.
   *
   * @return int
   *   The total items.
   */
  public function totalItems(): int {
    return $this->totalItems;
  }

}
