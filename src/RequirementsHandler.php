<?php

namespace Drupal\dubbot;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handler class to manage DubBot module requirements.
 */
class RequirementsHandler implements ContainerInjectionInterface {

  use DubBotConfigTrait;
  use StringTranslationTrait;

  /**
   * The DubBot Client service.
   *
   * @var \Drupal\dubbot\ClientInterface
   */
  protected $dubbotClient;

  /**
   * Constructs a new RequirementsHandler instance.
   *
   * @param \Drupal\dubbot\ClientInterface $dubbot_client
   *   The DubBot Client service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ClientInterface $dubbot_client, ConfigFactoryInterface $config_factory) {
    $this->dubbotClient = $dubbot_client;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('dubbot.client'),
      $container->get('config.factory')
    );
  }

  /**
   * Hook bridge.
   *
   * @return array
   *   The dubbot requirements array.
   *
   * @see dubbot_requirements()
   */
  public function requirements(string $phase): array {
    if ($phase !== 'runtime') {
      return [];
    }

    $description = $this->t('DubBot Embed key can be set from the @link.', ['@link' => Link::createFromRoute('DubBot settings page', 'dubbot.settings')->toString()]);
    $severity = REQUIREMENT_INFO;
    $key = $this->dubbotEmbedKey();

    if (!$key) {
      $value = $this->t('DubBot Embed Key has not been set.');
    }
    else {
      try {
        if ($this->dubbotClient->isValidEmbedKey($key)) {
          $value = $this->t('Valid DubBot Embed key set.');
          $severity = REQUIREMENT_OK;
        }
        else {
          $value = $this->t('Invalid DubBot Embed key set.');
          $severity = REQUIREMENT_ERROR;
        }
      }
      catch (\Exception $e) {
        $value = $this->t('Unable to validate the embed key. Please try again or contact the site administrator.');
        $severity = REQUIREMENT_WARNING;
      }
    }

    $requirements['dubbot'] = [
      'title' => $this->t('DubBot Embed Key'),
      'value' => $value,
      'description' => $description,
      'severity' => $severity,
    ];

    return $requirements;
  }

}
