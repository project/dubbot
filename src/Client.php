<?php

namespace Drupal\dubbot;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

/**
 * Client for sending HTTP requests to DubBot.
 *
 * @internal
 */
class Client implements ClientInterface {

  use DubBotConfigTrait;
  use LoggerChannelTrait;
  use StringTranslationTrait;
  use UseCacheBackendTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The DubBot domain negotiator service.
   *
   * @var \Drupal\dubbot\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * Boolean indicating whether the DubBot integration is enabled or not.
   *
   * @var bool
   */
  protected $enabled;

  /**
   * Constructs a Client object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend service.
   * @param \Drupal\dubbot\DomainNegotiatorInterface $domain_negotiator
   *   The DubBot domain negotiator service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory service.
   */
  public function __construct(GuzzleClientInterface $client, ConfigFactoryInterface $config_factory, CacheBackendInterface $cache_backend, DomainNegotiatorInterface $domain_negotiator, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $client;
    $this->configFactory = $config_factory;
    $this->cacheBackend = $cache_backend;
    $this->domainNegotiator = $domain_negotiator;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    if (isset($this->enabled)) {
      return $this->enabled;
    }

    $cache = $this->cacheGet(static::CLIENT_CACHE_ENABLED_CID);
    if (isset($cache->data)) {
      $this->enabled = $cache->data;
      return $this->enabled;
    }

    $key = $this->dubbotEmbedKey();
    $this->enabled = !empty($key) && $this->isValidEmbedKey($key);
    $this->cacheSet(static::CLIENT_CACHE_ENABLED_CID, $this->enabled, Cache::PERMANENT, [static::CLIENT_CACHE_TAG]);
    return $this->enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function isValidEmbedKey(string $embed_key): bool {
    $url = $this->buildUrl(static::EMBED_ENDPOINT_PATH, [], $embed_key);
    try {
      $response = $this->httpClient->get($url, [
        'headers' => [
          'Accept' => 'application/json',
        ],
      ]);
    }
    catch (GuzzleException $e) {
      $this->logError('Validate Key', $e);

      // BadResponseExceptions are returned as FALSE.
      if ($e instanceof BadResponseException) {
        return FALSE;
      }

      throw $e;
    }

    return $response->getStatusCode() == 200;
  }

  /**
   * Builds a valid DubBot endpoint URL.
   *
   * @param string $path
   *   The endpoint path.
   * @param array $query
   *   (Optional) Array containing the query parameters to include.
   * @param string $embed_key
   *   (Optional) The DubBot embed key to use. Defaults to the stored one.
   *
   * @return string|null
   *   The endpoint URL for the given path. NULL if unable to build it.
   */
  protected function buildUrl(string $path, array $query = [], string $embed_key = NULL): ?string {
    if (!isset($embed_key)) {
      $embed_key = $this->dubbotEmbedKey();
      if (empty($embed_key)) {
        return NULL;
      }
    }

    $replacements = [
      ':embed_key' => $embed_key,
    ];

    $url = static::API_BASE_URL . UrlHelper::encodePath(strtr($path, $replacements));

    // Check tab permsissions.
    $perms = [
      'best-practices' => 'view dubbot practices tab',
      'web-governance' => 'view dubbot governance tab',
      'a11y' => 'view dubbot accessibility tab',
      'spelling' => 'view dubbot spellcheck tab',
      'seo' => 'view dubbot seo tab',
      'links' => 'view dubbot links tab',
    ];
    $tabs = [];

    foreach ($perms as $tab => $perm) {
      if (\Drupal::currentUser()->hasPermission($perm)) {
        $tabs[] = $tab;
      }
    };

    if (count($tabs)) {
      $query['show'] = implode(',', $tabs);
    }

    if (empty($query)) {
      return $url;
    }

    $query = UrlHelper::buildQuery($query);
    return $url . '?' . $query;
  }

  /**
   * {@inheritdoc}
   */
  public function iframeReportUrlByUrl(string $url): ?string {
    return $this->buildUrl(static::EMBED_ENDPOINT_PATH, ['url' => $url]);
  }

  /**
   * {@inheritdoc}
   */
  public function iframeReportUrlByPageId(string $page_id): ?string {
    return $this->buildUrl(static::EMBED_ENDPOINT_PATH, ['page_id' => $page_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function reportByUrl(string $url): DubBotEmbedJsonResponse {
    $iframe_url = $this->iframeReportUrlByUrl($url);

    return $this->report($iframe_url);
  }

  /**
   * {@inheritdoc}
   */
  public function reportByPageId(string $page_id): DubBotEmbedJsonResponse {
    $iframe_url = $this->iframeReportUrlByPageId($page_id);

    return $this->report($iframe_url);
  }

  /**
   * Returns the JSON response for a given iframe URL.
   *
   * @param string $iframe_url
   *   The iframe URL.
   *
   * @return \Drupal\dubbot\DubBotEmbedJsonResponse
   *   The report resopnse.
   */
  protected function report(string $iframe_url): DubBotEmbedJsonResponse {
    try {
      $response = $this->httpClient->get($iframe_url, [
        'headers' => [
          'Accept' => 'application/json',
        ],
      ]);

      return DubBotEmbedJsonResponse::fromHttpClientResponse($response);
    }
    catch (GuzzleException $e) {
      $this->logError('Page Report', $e);

      // BadResponseExceptions are returned as DubBotEmbedJsonResponse.
      if ($e instanceof BadResponseException) {
        return DubBotEmbedJsonResponse::fromHttpClientResponse($e->getResponse());
      }

      throw $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function overview(int $page, int $limit, string $sort = 'title', string $order = 'asc'): object {
    $url = $this->buildUrl(static::PAGES_ENDPOINT_PATH);

    try {
      $response = $this->httpClient->post($url, [
        'headers' => [
          'Accept' => 'application/json',
        ],
        RequestOptions::JSON => [
          'domains' => $this->domainNegotiator->domains(),
          'page' => $page,
          'limit' => $limit,
          'sort' => $sort,
          'order' => $order,
        ],
      ]);
    }
    catch (GuzzleException $e) {
      $this->logError('Overview', $e);

      throw $e;
    }

    return DubBotPagesJsonResponse::fromHttpClientResponse($response);
  }

  /**
   * Logs DubBot client exceptions.
   *
   * @param string $operation
   *   The DubBot operation being executed.
   * @param \GuzzleHttp\Exception\GuzzleException $exception
   *   The exception to log.
   */
  protected function logError(string $operation, GuzzleException $exception): void {
    $params = [
      '%operation' => $operation,
      '%message' => $exception->getMessage(),
    ];
    $this->getLogger('dubbot')
      ->error('Error performing operation %operation: %message', $params);
  }

}
