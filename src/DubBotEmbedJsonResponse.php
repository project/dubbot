<?php

namespace Drupal\dubbot;

use Drupal\Component\Serialization\Json;
use Psr\Http\Message\ResponseInterface;

/**
 * Maps the DubBot Embed JSON response.
 */
final class DubBotEmbedJsonResponse {

  /**
   * The HTTP status code.
   *
   * @var int
   */
  private $code;

  /**
   * The page ID.
   *
   * @var string|null
   */
  private $pageId;

  /**
   * The number of issues found in the page.
   *
   * @var int|null
   */
  private $issuesCount;

  /**
   * The date the page was crawled.
   *
   * @var \DateTime|null
   */
  private $crawledAt;

  /**
   * The error message.
   *
   * @var string|null
   */
  private $error;

  /**
   * Constructs a new DubBotEmbedJsonResponse instance.
   *
   * @param int $code
   *   The HTTP status code.
   * @param string|null $page_id
   *   (Optional) The page ID.
   * @param int|null $issues_count
   *   (Optional) The number of issues found in the page.
   * @param \DateTime|null $crawled_at
   *   (Optional) The date the page was crawled.
   * @param string|null $error
   *   (Optional) The error message.
   */
  public function __construct(int $code, string $page_id = NULL, int $issues_count = NULL, \DateTime $crawled_at = NULL, string $error = NULL) {
    $this->code = $code;
    $this->pageId = $page_id;
    $this->issuesCount = $issues_count;
    $this->crawledAt = $crawled_at;
    $this->error = $error;
  }

  /**
   * Builds a new DubBotEmbedJsonResponse from a HTTP client response object.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The HTTP response.
   *
   * @return self
   *   The DubBotEmbedJsonResponse instance.
   */
  public static function fromHttpClientResponse(ResponseInterface $response): self {
    $body = Json::decode($response->getBody());
    return new static(
      (int) $response->getStatusCode(),
      $body['page_id'] ?? NULL,
      $body['total_issues_count'] ?? NULL,
      isset($body['crawled_at']) ? new \DateTime($body['crawled_at']) : NULL,
      $body['error'] ?? NULL,
    );
  }

  /**
   * Returns the response code.
   *
   * @return int
   *   The response code.
   */
  public function code(): int {
    return $this->code;
  }

  /**
   * Returns the page ID.
   *
   * @return string|null
   *   The page ID if set. NULL otherwise.
   */
  public function pageId(): ?string {
    return $this->pageId;
  }

  /**
   * Returns the number of issues found.
   *
   * @return int|null
   *   The number of issues if set. NULL otherwise.
   */
  public function issuesCount(): ?int {
    return $this->issuesCount;
  }

  /**
   * Returns the crawl date.
   *
   * @return \DateTime|null
   *   The crawled time if set. NULL otherwise.
   */
  public function crawledAt(): ?\DateTime {
    return $this->crawledAt;
  }

  /**
   * Returns the error message.
   *
   * @return string|null
   *   The error message if set. NULL otherwise.
   */
  public function error(): ?string {
    return $this->error;
  }

  /**
   * Checks if the DubBot response is valid.
   *
   * @return bool
   *   TRUE if the DubBot response is valid. FALSE otherwise.
   */
  public function isValidResponse(): bool {
    return $this->code === 200;
  }

}
