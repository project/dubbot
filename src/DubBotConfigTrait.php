<?php

namespace Drupal\dubbot;

use Drupal\Core\Config\ImmutableConfig;

/**
 * Shortcut trait to access to DubBot settings configuration object.
 */
trait DubBotConfigTrait {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The DubBot settings object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Return the DubBot settings config object.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The DubBot settings config object.
   */
  protected function config(): ImmutableConfig {
    if (!isset($this->configFactory)) {
      $this->configFactory = \Drupal::configFactory();
    }
    if (!isset($this->config)) {
      $this->config = $this->configFactory->get('dubbot.settings');
    }

    return $this->config;
  }

  /**
   * Shortcut to get the DubBot embed key.
   *
   * @return string|null
   *   The embed key if set. NULL otherwise.
   */
  protected function dubbotEmbedKey(): ?string {
    return $this->config()->get('embed_key');
  }

  /**
   * Shortcut to get the DubBot dialog renderer method.
   *
   * @return string|null
   *   The embed key if set. NULL otherwise.
   */
  protected function dubbotDialogRenderer(): ?string {
    return $this->config()->get('dialog_renderer');
  }

}
