<?php

namespace Drupal\dubbot;

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Maps the DubBot Page JSON response.
 */
final class DubBotPageJsonResponse {

  /**
   * The site ID.
   *
   * @var string
   */
  private $siteId;

  /**
   * The page ID.
   *
   * @var string
   */
  private $id;

  /**
   * The page URL.
   *
   * @var string
   */
  private $path;

  /**
   * The page title.
   *
   * @var string
   */
  private $title;

  /**
   * The number of issues found in the page.
   *
   * @var int
   */
  private $issuesCount;

  /**
   * The date the page was crawled.
   *
   * @var \DateTime
   */
  private $crawledAt;

  /**
   * Constructs a new DubBotPageJsonResponse instance.
   *
   * @param string $site_id
   *   The site ID.
   * @param string $id
   *   The page ID.
   * @param string $url
   *   The page URL.
   * @param string $title
   *   The page title.
   * @param int $issues_count
   *   The number of issues found in the page.
   * @param \DateTime $crawled_at
   *   The date the page was crawled.
   */
  public function __construct(string $site_id, string $id, string $url, string $title, int $issues_count, \DateTime $crawled_at) {
    $this->siteId = $site_id;
    $this->id = $id;
    $this->path = $url;
    $this->title = $title;
    $this->issuesCount = $issues_count;
    $this->crawledAt = $crawled_at;
  }

  /**
   * Builds a new DubBotPageJsonResponse from a formatted array.
   *
   * @param array $page
   *   The page object in array format.
   *
   * @return self
   *   The DubBotPageJsonResponse instance.
   */
  public static function fromArray(array $page): self {
    return new static(
      $page['site_id'],
      $page['id'],
      $page['path'],
      $page['title'],
      $page['total_issues_count'],
      new \DateTime($page['crawled_at']),
    );
  }

  /**
   * Returns the site ID.
   *
   * @return string
   *   The site ID.
   */
  public function siteId(): string {
    return $this->siteId;
  }

  /**
   * Returns the page ID.
   *
   * @return string
   *   The page ID.
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * Returns the page URL.
   *
   * @return string
   *   The page URL.
   */
  public function url(): string {
    return $this->path;
  }

  /**
   * Returns the page link.
   *
   * @return \Drupal\Core\Link
   *   The page link.
   */
  public function link(): Link {
    return Link::fromTextAndUrl($this->title(), Url::fromUri($this->url()));
  }

  /**
   * Returns the page title.
   *
   * @return string
   *   The page title.
   */
  public function title(): string {
    return $this->title;
  }

  /**
   * Returns the number of issues found.
   *
   * @return int
   *   The number of issues.
   */
  public function issuesCount(): int {
    return $this->issuesCount;
  }

  /**
   * Returns the crawl date.
   *
   * @return \DateTime
   *   The crawled time.
   */
  public function crawledAt(): \DateTime {
    return $this->crawledAt;
  }

  /**
   * Array representation of the page.
   *
   * @return array
   *   The array representation of the page.
   */
  public function toArray(): array {
    return [
      'site_id' => $this->siteId(),
      'id' => $this->id(),
      'path' => $this->url(),
      'title' => $this->title(),
      'total_issues_count' => $this->issuesCount(),
      'crawled_at' => $this->crawledAt()->format('Y-m-d\TH:i:s.v'),
    ];
  }

}
