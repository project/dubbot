<?php

namespace Drupal\dubbot;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a class which generates a link to DubBot reports.
 */
class LinkGenerator implements LinkGeneratorInterface {

  use DubBotConfigTrait;
  use StringTranslationTrait;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The DubBot Client service.
   *
   * @var \Drupal\dubbot\ClientInterface
   */
  protected $client;

  /**
   * The URL to reference from the link item.
   *
   * @var string
   */
  protected $url;

  /**
   * Contains the DubBot report static cache.
   *
   * @var array
   */
  protected $dubbotReports = [];

  /**
   * Constructs a new LinkGenerator instance.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\dubbot\ClientInterface $client
   *   The DubBot Client service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(RequestStack $request_stack, ClientInterface $client, ConfigFactoryInterface $config_factory) {
    $this->requestStack = $request_stack;
    $this->client = $client;
    $this->configFactory = $config_factory;
  }

  /**
   * Sets the toolbar item URL.
   *
   * @param string $url
   *   The URL to reference from the toolbar item.
   */
  public function setUrl(string $url): void {
    $this->url = $url;
  }

  /**
   * Gets the toolbar item URL.
   *
   * @return string
   *   The toolbar item URL. Defaults to the current URL.
   */
  public function getUrl(): string {
    if (!isset($this->url)) {
      $this->url = Url::fromUserInput($this->requestStack->getCurrentRequest()->getRequestUri(), ['absolute' => TRUE])->toString();
    }

    return $this->url;
  }

  /**
   * Generates a DubBot report link.
   *
   * @param array $classes
   *   Array containing extra classes to add to the link.
   *
   * @return \Drupal\Core\Link|null
   *   The DubBot link if available. NULL otherwise.
   */
  public function generate(array $classes = []): ?Link {
    try {
      $dubbot_response = $this->dubbotReport($this->getUrl());
    }
    catch (\Exception $e) {
      return NULL;
    }

    $url = $this->getLinkUrl($dubbot_response);
    $url->setOption('attributes', [
      'title' => $this->t('DubBot Report'),
      'class' => $this->getClasses($dubbot_response, $classes),
      'data-dialog-type' => 'dialog',
      'data-dialog-renderer' => $this->dubbotDialogRenderer(),
      'data-dialog-options' => Json::encode([
        'width' => 540,
        'height' => 500,
      ]),
    ]);

    return Link::fromTextAndUrl($this->getLinkText($dubbot_response), $url);
  }

  /**
   * Builds the classes array.
   *
   * @param \Drupal\dubbot\DubBotEmbedJsonResponse $response
   *   The DubBot report response.
   * @param array $classes
   *   Array containing extra classes to add to the link.
   *
   * @return string[]
   *   The classes array for the item.
   */
  protected function getClasses(DubBotEmbedJsonResponse $response, array $classes): array {
    $classes = array_merge($classes, ['use-ajax', 'dubbot-report-link']);
    $status_class = $response->isValidResponse() ? 'dubbot-enabled' : 'dubbot-disabled';
    $classes[] = $status_class;

    if ($response->isValidResponse()) {
      $count_class = ($response->issuesCount() > 0) ? 'dubbot-has-issues' : 'dubbot-has-no-issues';
      $classes[] = $count_class;
    }

    return $classes;
  }

  /**
   * Builds the DubBot Toolbar item URL.
   *
   * @param \Drupal\dubbot\DubBotEmbedJsonResponse $response
   *   The DubBot report response.
   *
   * @return \Drupal\Core\Url
   *   The URL object to include in the DubBot toolbar item.
   */
  protected function getLinkUrl(DubBotEmbedJsonResponse $response): Url {
    if ($response->isValidResponse()) {
      return Url::fromRoute('dubbot.report', ['page_id' => $response->pageId()]);
    }
    return Url::fromRoute('<nolink>');
  }

  /**
   * Builds the DubBot Toolbar item text.
   *
   * @param \Drupal\dubbot\DubBotEmbedJsonResponse $response
   *   The DubBot report response.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The text to include in the DubBot toolbar item.
   */
  protected function getLinkText(DubBotEmbedJsonResponse $response): TranslatableMarkup {
    $count = $response->isValidResponse() ? $response->issuesCount() : $this->t('N/A');

    return $this->t('DubBot (@count)', ['@count' => $count]);
  }

  /**
   * Retrieves the DubBot report for the given URL from the static cache.
   *
   * @param string $url
   *   The Report URL.
   *
   * @return \Drupal\dubbot\DubBotEmbedJsonResponse
   *   The DubBotEmbedJsonResponse for the given URL.
   */
  protected function dubbotReport(string $url): DubBotEmbedJsonResponse {
    $hash = Crypt::hashBase64($url);

    if (!isset($this->dubbotReports[$hash])) {
      $this->dubbotReports[$hash] = $this->client->reportByUrl($url);
    }

    return $this->dubbotReports[$hash];
  }

}
