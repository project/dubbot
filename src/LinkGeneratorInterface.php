<?php

namespace Drupal\dubbot;

use Drupal\Core\Link;

/**
 * Interface that generates a link to DubBot reports.
 */
interface LinkGeneratorInterface {

  const LINK_CACHE_TAG = 'dubbot:link';

  /**
   * Sets the toolbar item URL.
   *
   * @param string $url
   *   The URL to reference from the toolbar item.
   */
  public function setUrl(string $url): void;

  /**
   * Gets the toolbar item URL.
   *
   * @return string
   *   The toolbar item URL. Defaults to the current URL.
   */
  public function getUrl(): string;

  /**
   * Generates a DubBot report link.
   *
   * @param array $classes
   *   Array containing extra classes to add to the link.
   *
   * @return \Drupal\Core\Link|null
   *   The DubBot link if available. NULL otherwise.
   */
  public function generate(array $classes = []): ?Link;

}
