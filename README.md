CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

This module provides integration with the [DubBot](https://dubbot.com/) service.

[DubBot](https://dubbot.com/) provides automated tests for website accessibility,
broken links, spelling, and customizable web governance rules. Simply put,
DubBot is easy to use, powerful software that helps organizations optimize their
websites for an inclusive user experience.

This module allows to integrate DubBot inspection results directly in Drupal
sites. The main features are listed below:

* Overview page where all the crawled pages are listed, including the number of
  issues and links to more in detail individual reports
* Generic block that can be placed anywhere and opens the report directly using
  the Drupal off canvas tray or a traditinal modal
* Toolbar integration that adds a DubBot specific section in the Drupal admin
  toolbar, from where the in-page reports can be opened

* For a full description of the module visit:
  https://www.drupal.org/project/dubbot

* To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/dubbot

REQUIREMENTS
------------
* DubBot
  * DubBot account properly configured to track the Drupal site domain
  * DubBot embed key that can be generated form the DubBot account settings
* Drupal
  * This module requires no modules outside of Drupal core.
  * Toolbar module is required to enable the Toolbar inegration.

INSTALLATION
------------

Install the DubBot module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

### Set the DubBot Embed key
Visit the DubBot settings page `/admin/config/content/dubbot/settings` and
enter the provided embed key to enable the integration.

### Set the Dubbot API URL
This is to allow highlighting and other items to be loaded from the API. This
defaults to https://api.dubbot.com however one may receive a different endpoint.

### Configure permissions
Configure the module permissions:

* **Administer DubBot configuration**: To manage the DubBot embed keys and
  general module configuration
* **Access DubBot report**: To access to the global overview page and the
  different individual report links
* **Access Individual panes**: Limit which panes a user role can view. Each role
  can be granted permission. If no permissions are selected, all panes are
  visible.

### Create a DubBot report block
Access to the Block Layout page and create a new block of type `DubBot Report`,
define the link color and place it in the page. It will automatically will
include the DubBot report for the current page if available.

### Enable Toolbar integration
Toolbar integration requires to install the `DubBot Toolbar`submodule. Once
installed, a new toolbar icon linking to the individual page report will be
available for granted users.

MAINTAINERS
-----------
Development:

* Pablo López Escobés ([plopesc](https://dgo.to/u/plopesc))
* Mark Casias ([markie](https://dgo.to/u/markie))

Supporting organizations:

* DubBot - https://dubbot.com
* Lullabot - https://www.drupal.org/lullabot
* Tugboat - https://www.tugboatqa.com/
