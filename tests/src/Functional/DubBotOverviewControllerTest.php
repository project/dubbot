<?php

namespace Drupal\Tests\dubbot\Functional;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\dubbot\ClientInterface;
use Drupal\dubbot_test\MockHttpClient;
use Drupal\Tests\BrowserTestBase;

/**
 * @covers \Drupal\dubbot\Controller\DubBotOverviewController
 * @group dubbot
 */
class DubBotOverviewControllerTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dubbot',
    'dubbot_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create and log in user.
    $this->drupalLogin($this->drupalCreateUser([
      'access dubbot report',
    ]));

    // Set the enabled embed key flag to TRUE to bypass checking.
    \Drupal::cache()->set(
      ClientInterface::CLIENT_CACHE_ENABLED_CID,
      TRUE,
      Cache::PERMANENT,
      [ClientInterface::CLIENT_CACHE_TAG]
    );

    // Set the endpoint response mock callback.
    \Drupal::service('state')
      ->set(MockHttpClient::MOCK_HTTP_CLIENT_RESPONSE,
        [MockHttpClient::class, 'buildOverviewResponse']
      );
  }

  /**
   * Tests paging.
   */
  public function testPager(): void {
    // Load the listing page.
    $this->drupalGet('/admin/config/content/dubbot');

    // Item 51 should not be present.
    $this->assertSession()->pageTextContains('Page 50');
    $this->assertSession()->responseNotContains('Page 51');

    // Browse to the next page, test entity 51 is shown.
    $this->clickLink('Page 2');
    $this->assertSession()->responseNotContains('Page 50');
    $this->assertSession()->pageTextContains('Page 51');
  }

  /**
   * Tests paging.
   */
  public function testSort(): void {
    // Load the listing page.
    $this->drupalGet('/admin/config/content/dubbot');

    $this->clickLink('Issues Count');
    $this->assertSession()->elementTextEquals('xpath', '//table/tbody/tr[1]/td[2]', '51');
    $this->clickLink('Issues Count');
    $this->assertSession()->elementTextEquals('xpath', '//table/tbody/tr[1]/td[2]', '1');

    $this->clickLink('Crawled At');
    $this->assertSession()->elementTextEquals('xpath', '//table/tbody/tr[1]/td[3]', 'Wed, 01/01/2020 - 11:51');
    $this->clickLink('Crawled At');
    $this->assertSession()->elementTextEquals('xpath', '//table/tbody/tr[1]/td[3]', 'Wed, 01/01/2020 - 11:01');

    $this->clickLink('Page');
    $this->assertSession()->elementTextEquals('xpath', '//table/tbody/tr[1]/td[1]', 'Page 01');
    $this->clickLink('Page');
    $this->assertSession()->elementTextEquals('xpath', '//table/tbody/tr[1]/td[1]', 'Page 51');
  }

  /**
   * Tests if the list cache tags are set.
   */
  public function testCacheTags(): void {
    $this->drupalGet('/admin/config/content/dubbot');
    $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', ClientInterface::CLIENT_CACHE_TAG);
  }

  /**
   * Tests empty table response.
   */
  public function testErrorResponse(): void {
    \Drupal::state()
      ->set(MockHttpClient::MOCK_HTTP_CLIENT_RESPONSE, new \Exception('Bad Response'));
    $this->drupalGet('/admin/config/content/dubbot');

    $this->assertSession()->pageTextContains('There are no pages crawled by DubBot yet.');
  }

  /**
   * Tests DubBot unavailable error message.
   */
  public function testDubBotDisabled(): void {
    Cache::invalidateTags([ClientInterface::CLIENT_CACHE_TAG]);

    $this->drupalGet('/admin/config/content/dubbot');

    $this->assertSession()->statusMessageExists(MessengerInterface::TYPE_ERROR);
    $this->assertSession()->statusMessageContains('DubBot Embed key is not set or is not valid. Please review it in the Settings page.');
  }

}
