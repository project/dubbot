<?php

namespace Drupal\Tests\dubbot\Functional;

use Drupal\dubbot_test\MockHttpClient;
use Drupal\Tests\BrowserTestBase;
use GuzzleHttp\Psr7\Response;

/**
 * @covers \Drupal\dubbot\Form\SettingsForm
 * @group dubbot
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dubbot',
    'dubbot_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $user = $this->drupalCreateUser(['administer dubbot configuration']);
    $user->save();
    $this->drupalLogin($user);
  }

  /**
   * Checks form access.
   */
  public function testAccess(): void {
    $this->drupalGet('/admin/config/content/dubbot/settings');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->titleEquals('DubBot Settings | Drupal');
    $this->assertSession()->fieldExists('embed_key');
    $this->assertSession()->fieldExists('dialog_renderer');

    $this->drupalLogout();
    $this->drupalGet('/admin/config/content/dubbot/settings');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Checks form validation logic.
   */
  public function testValidate(): void {
    $state = \Drupal::service('state');
    $state->set(MockHttpClient::MOCK_HTTP_CLIENT_RESPONSE, new Response(500));
    $embed_key = $this->randomMachineName();

    $this->drupalGet('/admin/config/content/dubbot/settings');
    $this->submitForm(['embed_key' => $embed_key], 'Save configuration');
    $this->assertNotEquals($embed_key, $this->config('dubbot.settings')->get('embed_key'));
    $this->assertSession()->statusMessageContains('The embed key is not valid.');

    $state->set(MockHttpClient::MOCK_HTTP_CLIENT_RESPONSE, new \Exception('Bad Response'));
    $this->submitForm(['embed_key' => $embed_key], 'Save configuration');
    $this->assertNotEquals($embed_key, $this->config('dubbot.settings')->get('embed_key'));
    $this->assertSession()->statusMessageContains('Unable to validate the embed key. Please try again or contact the site administrator.');
  }

  /**
   * Checks form submission logic.
   */
  public function testSave(): void {
    $embed_key = $this->randomMachineName();
    $this->assertNotEquals($embed_key, $this->config('dubbot.settings')->get('embed_key'));
    $this->drupalGet('/admin/config/content/dubbot/settings');
    $this->assertSession()->fieldValueEquals('embed_key', '');
    $this->assertSession()->fieldValueEquals('dialog_renderer', 'off_canvas');
    $this->submitForm([
      'embed_key' => $embed_key,
      'dialog_renderer' => 'off_canvas_top',
    ], 'Save configuration');
    $this->assertEquals($embed_key, $this->config('dubbot.settings')->get('embed_key'));
    $this->assertSession()->statusMessageContains('The configuration options have been saved.');
    $this->assertSession()->fieldValueEquals('embed_key', $embed_key);
    $this->assertSession()->fieldValueEquals('dialog_renderer', 'off_canvas_top');
  }

}
