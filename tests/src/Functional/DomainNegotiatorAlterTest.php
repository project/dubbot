<?php

namespace Drupal\Tests\dubbot\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * @covers \Drupal\dubbot\DomainNegotiator
 * @group dubbot
 */
class DomainNegotiatorAlterTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['dubbot', 'dubbot_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests execution order of hook_form_alter() and hook_form_FORM_ID_alter().
   */
  public function testExecutionOrder() {
    $domains = \Drupal::service('dubbot.domain_negotiator')->domains();
    $this->assertCount(3, $domains);
    $this->assertContains('http://foo.com', $domains);
    $this->assertContains('https://bar.com', $domains);
  }

}
