<?php

namespace Drupal\Tests\dubbot\Functional;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\dubbot\ClientInterface;
use Drupal\dubbot_test\MockHttpClient;
use Drupal\Tests\BrowserTestBase;

/**
 * @covers \Drupal\dubbot\Plugin\Block\DubBotReportBlock
 * @group dubbot
 */
class DubBotReportBlockTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'dubbot',
    'dubbot_test',
    'test_page_test',
  ];

  /**
   * A list of theme regions to test.
   *
   * @var array
   */
  protected $regions;

  /**
   * A test user with administrative privileges.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Use the test page as the front page.
    $this->config('system.site')->set('page.front', '/test-page')->save();

    // Create and log in an administrative user having access to the Full HTML
    // text format.
    $this->adminUser = $this->drupalCreateUser([
      'administer blocks',
      'access administration pages',
      'access dubbot report',
    ]);
    $this->drupalLogin($this->adminUser);

    \Drupal::cache()->set(ClientInterface::CLIENT_CACHE_ENABLED_CID, TRUE);

    // Set the endpoint response mock callback.
    \Drupal::service('state')
      ->set(MockHttpClient::MOCK_HTTP_CLIENT_RESPONSE,
        [MockHttpClient::class, 'buildReportResponse']
      );
    \Drupal::configFactory()->getEditable('dubbot.settings')->set('embed_key', $this->randomMachineName())->save();

    // Define the existing regions.
    $this->regions = [
      'header',
      'sidebar_first',
      'content',
      'sidebar_second',
      'footer',
    ];
    $block_storage = $this->container->get('entity_type.manager')->getStorage('block');
    $blocks = $block_storage->loadByProperties(['theme' => $this->config('system.theme')->get('default')]);
    foreach ($blocks as $block) {
      $block->delete();
    }
  }

  /**
   * Tests block settings.
   */
  public function testBlockSettings() {
    $block_name = 'dubbot_report';
    // Create a random title for the block.
    $title = $this->randomMachineName(8);
    // Create a random id for the block.
    $id = strtolower($this->randomMachineName(8));
    // Enable a standard block.
    $default_theme = $this->config('system.theme')->get('default');
    $edit = [
      'id' => $id,
      'region' => 'sidebar_first',
      'settings[label]' => $title,
      'settings[label_display]' => TRUE,
    ];
    $this->drupalGet('admin/structure/block/add/' . $block_name . '/' . $default_theme);

    $this->submitForm($edit, 'Save block');
    $this->assertSession()->statusMessageContains('The block configuration has been saved.', 'status');

    $this->drupalGet('admin/structure/block/manage/' . $id);
    $edit['settings[link_color]'] = $this->randomMachineName();

    $this->submitForm($edit, 'Save block');
    $this->assertSession()->statusMessageContains('DubBot link color cannot be longer than 7 characters but is currently 8 characters long.', MessengerInterface::TYPE_ERROR);

    $edit['settings[link_color]'] = '#YesNo0';
    $this->submitForm($edit, 'Save block');
    $this->assertSession()->statusMessageContains('Please enter a valid HEX color code.', MessengerInterface::TYPE_ERROR);

    $edit['settings[link_color]'] = '#fabada';
    $this->submitForm($edit, 'Save block');
    $this->assertSession()->statusMessageContains('The block configuration has been saved.', 'status');

    $this->drupalGet('admin/structure/block/manage/' . $id);
    $this->assertSession()->fieldValueEquals('settings[link_color]', '#fabada');

    // Confirm that the block is displayed on the front page.
    $this->drupalGet('');
    $this->assertSession()->pageTextContains($title);

    $this->assertSession()->elementAttributeContains('css', '.dubbot-report-block.dubbot-block', 'data-drupal-dubbot-color', '#fabada');
    $this->assertSession()->elementAttributeContains('css', '.dubbot-report-block.dubbot-block', 'data-drupal-dubbot-selector', '.dubbot-report-block.dubbot-block .dubbot-report-link');
  }

}
