<?php

namespace Drupal\Tests\dubbot\Unit;

use Drupal\Component\Serialization\Json;
use Drupal\dubbot\DubBotPageJsonResponse;
use Drupal\dubbot\DubBotPagesJsonResponse;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Psr7\Response;

/**
 * @covers \Drupal\dubbot\DubBotPagesJsonResponse
 * @group dubbot
 */
class DubBotPagesJsonResponseTest extends UnitTestCase {

  /**
   * The pages array.
   *
   * @var \Drupal\dubbot\DubBotPageJsonResponse[]
   */
  protected $pages;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->pages = [];

    $site_id = $this->randomMachineName();
    for ($i = 0; $i < rand(1, 10); $i++) {
      $id = $this->randomMachineName();
      $url = 'https://example.com/test';
      $title = 'This is the page title';
      $issues_count = rand(0, 25);
      $date = new \Datetime('2023-05-05');
      $page = new DubBotPageJsonResponse($site_id, $id, $url, $title, $issues_count, $date);
      $this->pages[] = $page;
    }
  }

  /**
   * Tests the class constructor.
   */
  public function testConstructor(): void {
    $current_page = rand(1, 10);
    $total_pages = rand(1, 10);
    $total_items = rand(1, 10);

    $response = new DubBotPagesJsonResponse($this->pages, $current_page, $total_pages, $total_items);

    $this->assertEquals($current_page, $response->currentPage());
    $this->assertEquals($total_pages, $response->totalPages());
    $this->assertEquals($total_items, $response->totalItems());
    $this->assertEquals(count($this->pages), count($response->pages()));

    foreach ($this->pages as $key => $page) {
      $this->assertEquals($page->id(), $response->pages()[$key]->id());
    }

    // Expect exception when pages are not DubBotPagesJsonResponse instances.
    $pages = ['not-a-page'];
    $this->expectException(\TypeError::class);
    new DubBotPagesJsonResponse($pages, $current_page, $total_pages, $total_items);
  }

  /**
   * Tests the class static factory method.
   */
  public function testFromHttpClientResponse(): void {
    $current_page = rand(1, 10);
    $total_pages = rand(1, 10);
    $total_items = rand(1, 10);
    $pagination = [
      'current_page' => $current_page,
      'total_pages' => $total_pages,
      'total_items' => $total_items,
    ];
    $pages = [];
    foreach ($this->pages as $page) {
      $pages[] = $page->toArray();
    }
    $body = [
      'pages' => $pages,
      'pagination' => (object) $pagination,
    ];

    $valid_response = new Response(200, [], Json::encode((object) $body));
    $invalid_response = new Response(404);

    $response = DubBotPagesJsonResponse::fromHttpClientResponse($valid_response);
    $this->assertEquals($current_page, $response->currentPage());
    $this->assertEquals($total_pages, $response->totalPages());
    $this->assertEquals($total_items, $response->totalItems());
    $this->assertEquals(count($this->pages), count($response->pages()));

    foreach ($this->pages as $key => $page) {
      $this->assertEquals($page->id(), $response->pages()[$key]->id());
    }

    // Expect exception for invalid responses.
    $this->expectException(\Error::class);
    DubBotPagesJsonResponse::fromHttpClientResponse($invalid_response);
  }

}
