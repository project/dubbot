<?php

namespace Drupal\Tests\dubbot\Unit;

use Drupal\dubbot\DubBotPageJsonResponse;
use Drupal\Tests\UnitTestCase;

/**
 * @covers \Drupal\dubbot\DubBotPageJsonResponse
 * @group dubbot
 */
class DubBotPageJsonResponseTest extends UnitTestCase {

  /**
   * Tests the class constructor.
   */
  public function testConstructor(): void {
    $site_id = $this->randomMachineName();
    $id = $this->randomMachineName();
    $url = 'https://example.com/test';
    $title = 'This is the page title';
    $issues_count = rand(0, 25);
    $date = new \Datetime('2023-05-05');

    $response = new DubBotPageJsonResponse($site_id, $id, $url, $title, $issues_count, $date);

    $this->assertEquals($site_id, $response->siteId());
    $this->assertEquals($id, $response->id());
    $this->assertEquals($url, $response->url());
    $this->assertEquals($title, $response->title());
    $this->assertEquals($issues_count, $response->issuesCount());
    $this->assertEquals($date, $response->crawledAt());

    $link = $response->toLink()->toString();
    $this->assertEquals($title, $link->getText());
    $this->assertEquals($url, $link->getUrl()->getUri());

    $expected = [
      'site_id' => $site_id,
      'id' => $id,
      'path' => $url,
      'title' => $title,
      'total_issues_count' => $issues_count,
      'crawled_at' => $date->format('Y-m-d\TH:i:s.v'),
    ];
    $this->assertEquals($expected, $response->toArray());
  }

  /**
   * Tests the class static factory method.
   */
  public function testFromArray(): void {
    $page = [
      'site_id' => $this->randomMachineName(),
      'id' => $this->randomMachineName(),
      'path' => 'https://example.com/test',
      'title' => 'This is the page title',
      'total_issues_count' => rand(0, 25),
      'crawled_at' => '2023-05-05T00:00:00.000',
    ];

    $response = DubBotPageJsonResponse::fromArray($page);

    $this->assertEquals($page['site_id'], $response->siteId());
    $this->assertEquals($page['id'], $response->id());
    $this->assertEquals($page['path'], $response->url());
    $this->assertEquals($page['title'], $response->title());
    $this->assertEquals($page['total_issues_count'], $response->issuesCount());
    $this->assertEquals($page['crawled_at'], $response->crawledAt()->format('Y-m-d\TH:i:s.v'));

    $link = $response->toLink()->toString();
    $this->assertEquals($page['title'], $link->getText());
    $this->assertEquals($page['path'], $link->getUrl()->getUri());

    $this->assertEquals($page, $response->toArray());
  }

}
