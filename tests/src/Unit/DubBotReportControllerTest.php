<?php

namespace Drupal\Tests\dubbot\Unit;

use Drupal\Component\Utility\UrlHelper;
use Drupal\dubbot\ClientInterface;
use Drupal\dubbot\Controller\DubBotReportController;
use Drupal\dubbot\DubBotEmbedJsonResponse;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @covers \Drupal\dubbot\Controller\DubBotReportController
 * @group dubbot
 */
class DubBotReportControllerTest extends UnitTestCase {

  /**
   * The valid page ID.
   *
   * @var string
   */
  protected $validId = 'validId';

  /**
   * The invalid page ID.
   *
   * @var string
   */
  protected $invalidId = 'invalidID';

  /**
   * The DubBot client.
   *
   * @var \Drupal\dubbot\ClientInterface
   */
  protected $dubbotClient;

  /**
   * The user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * The controller instance for testing purposes.
   *
   * @var \Drupal\dubbot\Controller\DubBotReportController
   */
  protected $controller;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->dubbotClient = $this->createMock('Drupal\dubbot\Client');

    $query = UrlHelper::buildQuery(['url' => $this->validId]);
    $this->dubbotClient->expects($this->any())
      ->method('iframeReportUrlByUrl')
      ->willReturn(ClientInterface::API_BASE_URL . ClientInterface::EMBED_ENDPOINT_PATH . '/' . $this->randomMachineName() . '?' . $query);

    $this->dubbotClient->expects($this->any())
      ->method('reportByPageId')
      ->willReturnCallback(function ($page_id) {
        if ($page_id === $this->validId) {
          return new DubBotEmbedJsonResponse(200, $this->randomMachineName(), 0);
        }

        if ($page_id === $this->invalidId) {
          return new DubBotEmbedJsonResponse(404);
        }

        throw new \Exception('exception');
      });

    $this->controller = new DubBotReportController($this->dubbotClient);
    $this->controller->setStringTranslation($this->getStringTranslationStub());

    $container = new ContainerBuilder();

    $this->user = $this->createMock('Drupal\Core\Session\AccountInterface');
    $container->set('current_user', $this->user);
    $cache_context_manager = $this->createMock('Drupal\Core\Cache\Context\CacheContextsManager');
    $cache_context_manager->expects($this->any())
      ->method('assertValidTokens')
      ->willReturn(TRUE);
    $container->set('cache_contexts_manager', $cache_context_manager);
    \Drupal::setContainer($container);
  }

  /**
   * Tests the access() method.
   *
   * @dataProvider dataProviderTestAccess
   */
  public function testAccess($enabled, $permission, $page_id, $allowed): void {
    $this->dubbotClient->expects($this->once())
      ->method('isEnabled')
      ->willReturn($enabled);

    $this->user->expects($this->atMost(1))
      ->method('hasPermission')
      ->willReturn($permission);

    $this->assertEquals($allowed, $this->controller->access($page_id)->isAllowed());
  }

  /**
   * Data provider for testAccess().
   */
  public function dataProviderTestAccess(): array {
    return [
      [
        FALSE,
        FALSE,
        $this->randomMachineName(),
        FALSE,
      ],
      [
        FALSE,
        TRUE,
        $this->validId,
        FALSE,
      ],
      [
        TRUE,
        FALSE,
        $this->randomMachineName(),
        FALSE,
      ],
      [
        TRUE,
        FALSE,
        $this->validId,
        FALSE,
      ],
      [
        TRUE,
        TRUE,
        $this->invalidId,
        FALSE,
      ],
      [
        TRUE,
        TRUE,
        $this->randomMachineName(),
        FALSE,
      ],
      [
        TRUE,
        TRUE,
        $this->validId,
        TRUE,
      ],
    ];
  }

  /**
   * Tests the build() method.
   */
  public function testBuild(): void {
    $result = $this->controller->build($this->validId);

    $this->assertEquals('iframe', $result['content']['#tag']);
    $this->assertArrayHasKey('src', $result['content']['#attributes']);
    $this->assertContains('dubbot-iframe', $result['content']['#attributes']['class']);
    $this->assertContains('dubbot/iframe', $result['content']['#attached']['library']);
  }

}
