<?php

namespace Drupal\Tests\dubbot\Unit;

use Drupal\dubbot\DomainNegotiator;
use Drupal\Tests\UnitTestCase;

/**
 * @covers \Drupal\dubbot\DomainNegotiator
 * @group dubbot
 */
class DomainNegotiatorTest extends UnitTestCase {

  /**
   * The DubBot domain negotiator.
   *
   * @var \Drupal\dubbot\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $request = $this->createMock('Symfony\Component\HttpFoundation\Request');
    $request->expects($this->once())
      ->method('getSchemeAndHttpHost')
      ->willReturn('https://dubbot.com');
    $request_stack = $this->createMock('Symfony\Component\HttpFoundation\RequestStack');
    $request_stack->expects($this->once())
      ->method('getCurrentRequest')
      ->willReturn($request);

    $module_handler = $this->createMock('Drupal\Core\Extension\ModuleHandlerInterface');

    $this->domainNegotiator = new DomainNegotiator($request_stack, $module_handler);
  }

  /**
   * Tests the domains() method.
   */
  public function testDomains(): void {
    $this->assertEquals(['https://dubbot.com'], $this->domainNegotiator->domains());
  }

}
