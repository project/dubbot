<?php

namespace Drupal\Tests\dubbot\Unit;

use Drupal\Component\Serialization\Json;
use Drupal\dubbot\DubBotEmbedJsonResponse;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Psr7\Response;

/**
 * @covers \Drupal\dubbot\DubBotEmbedJsonResponse
 * @group dubbot
 */
class DubBotEmbedJsonResponseTest extends UnitTestCase {

  /**
   * Tests the class constructor.
   */
  public function testConstructor(): void {
    $page_id = $this->randomMachineName();
    $issues_count = rand(0, 25);
    $date = new \Datetime('2023-05-05');
    $error_message = 'No page found in DubBot that matches https://syl.pizza';

    $response = new DubBotEmbedJsonResponse(200, $page_id, $issues_count, $date);

    $this->assertEquals(200, $response->code());
    $this->assertEquals($page_id, $response->pageId());
    $this->assertEquals($issues_count, $response->issuesCount());
    $this->assertEquals($date, $response->crawledAt());
    $this->assertNull($response->error());
    $this->assertTrue($response->isValidResponse());

    $response = new DubBotEmbedJsonResponse(404, NULL, NULL, NULL, $error_message);
    $this->assertEquals(404, $response->code());
    $this->assertNull($response->pageId());
    $this->assertNull($response->issuesCount());
    $this->assertNull($response->crawledAt());
    $this->assertEquals($error_message, $response->error());
    $this->assertFalse($response->isValidResponse());
  }

  /**
   * Tests the class static factory method.
   */
  public function testFromHttpClientResponse(): void {
    $crawled_at = new \Datetime('2023-05-05');
    $valid_body = [
      'page_id' => $this->randomMachineName(),
      'total_issues_count' => rand(0, 25),
      'crawled_at' => $crawled_at->format('Y-m-d\TH:i:s.u\Z'),
    ];
    $invalid_body = [
      'error' => 'No page found in DubBot that matches https://syl.pizza',
    ];
    $valid_response = new Response(200, [], Json::encode($valid_body));
    $invalid_response = new Response(404, [], Json::encode($invalid_body));

    $dubbot_response = DubBotEmbedJsonResponse::fromHttpClientResponse($valid_response);
    $this->assertEquals(200, $dubbot_response->code());
    $this->assertEquals($valid_body['page_id'], $dubbot_response->pageId());
    $this->assertEquals($valid_body['total_issues_count'], $dubbot_response->issuesCount());
    $this->assertEquals($crawled_at->format('Y-m-d'), $dubbot_response->crawledAt()->format('Y-m-d'));
    $this->assertNull($dubbot_response->error());
    $this->assertTrue($dubbot_response->isValidResponse());

    $dubbot_response = DubBotEmbedJsonResponse::fromHttpClientResponse($invalid_response);
    $this->assertEquals(404, $dubbot_response->code());
    $this->assertNull($dubbot_response->pageId());
    $this->assertNull($dubbot_response->issuesCount());
    $this->assertNull($dubbot_response->crawledAt());
    $this->assertEquals($invalid_body['error'], $dubbot_response->error());
    $this->assertFalse($dubbot_response->isValidResponse());
  }

}
