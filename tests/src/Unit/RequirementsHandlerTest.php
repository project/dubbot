<?php

namespace Drupal\Tests\dubbot\Unit;

use Drupal\Component\Utility\Html;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\dubbot\RequirementsHandler;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\Stub\ConsecutiveCalls;
use Prophecy\Argument;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @covers \Drupal\dubbot\RequirementsHandler
 * @group dubbot
 */
class RequirementsHandlerTest extends UnitTestCase {

  /**
   * The DubBot Client service.
   *
   * @var \Drupal\dubbot\ClientInterface
   */
  protected $dubbotClient;

  /**
   * The requirements handler instance.
   *
   * @var \Drupal\dubbot\RequirementsHandler
   */
  protected $requirementsHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    require_once $this->root . '/core/includes/install.inc';

    $this->dubbotClient = $this->createMock('Drupal\dubbot\Client');

    $embed_key = $this->randomMachineName(16);
    $config_factory = $this->getConfigFactoryStub([
      'dubbot.settings' => [
        'embed_key' => $embed_key,
      ],
    ]);

    $this->requirementsHandler = new RequirementsHandler($this->dubbotClient, $config_factory);
    $this->requirementsHandler->setStringTranslation($this->getStringTranslationStub());

    $container = new ContainerBuilder();
    $link_generator = $this->prophesize(LinkGeneratorInterface::class);
    $link_generator->generate(Argument::any(), Argument::any())->willReturn('<a href="/admin/config/content/dubbot">DubBot settings page</a>');
    // This line is necessary for Drupal < 10.1.
    $link_generator->generateFromLink(Argument::type(Link::class))->willReturn('<a href="/admin/config/content/dubbot">DubBot settings page</a>');
    $container->set('link_generator', $link_generator->reveal());
    \Drupal::setContainer($container);
  }

  /**
   * Tests the results based on the requirements phase.
   */
  public function testPhase(): void {
    $this->dubbotClient->expects($this->once())
      ->method('isValidEmbedKey')
      ->willReturn(TRUE);

    $this->assertEmpty($this->requirementsHandler->requirements('install'));
    $this->assertEmpty($this->requirementsHandler->requirements('update'));
    $this->assertNotEmpty($this->requirementsHandler->requirements('runtime'));
  }

  /**
   * Tests the requirements output.
   *
   * @dataProvider providerTestValidEmbedKey
   */
  public function testValidEmbedKey($value, $severity, $client_response): void {
    $this->assertTrue(TRUE);
    $expected = [
      'title' => 'DubBot Embed Key',
      'value' => $value,
      'description' => Html::escape('DubBot Embed key can be set from the <a href="/admin/config/content/dubbot">DubBot settings page</a>.'),
      'severity' => $severity,
    ];
    $this->dubbotClient->expects($this->once())
      ->method('isValidEmbedKey')
      ->will($client_response);

    $result = $this->requirementsHandler->requirements('runtime');

    array_walk($expected, function ($expected, $key) use ($result) {
      $actual = $result['dubbot'][$key];
      $actual = ($actual instanceof TranslatableMarkup) ? $actual->render() : $actual;
      $this->assertEquals($expected, $actual);
    });
  }

  /**
   * Data provider for testValidEmbedKey().
   */
  public function providerTestValidEmbedKey(): array {
    return [
      [
        'Valid DubBot Embed key set.',
        0,
        new ConsecutiveCalls([TRUE]),
      ],
      [
        'Unable to validate the embed key. Please try again or contact the site administrator.',
        1,
        $this->throwException(new \Exception()),
      ],
      [
        'Invalid DubBot Embed key set.',
        2,
        new ConsecutiveCalls([FALSE]),
      ],
    ];
  }

}
