<?php

namespace src\Unit;

use Drupal\dubbot\DubBotEmbedJsonResponse;
use Drupal\dubbot\LinkGenerator;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \Drupal\dubbot\LinkGenerator
 * @group dubbot
 */
class LikGeneratorTest extends UnitTestCase {

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The DubBot Client service.
   *
   * @var \Drupal\dubbot\ClientInterface
   */
  protected $dubbotClient;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The DubBot link generator.
   *
   * @var \Drupal\dubbot\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->requestStack = $this->createMock('Symfony\Component\HttpFoundation\RequestStack');
    $this->dubbotClient = $this->createMock('Drupal\dubbot\Client');

    $this->requestStack->expects($this->any())
      ->method('getCurrentRequest')
      ->willReturn(new Request());

    $this->dubbotClient->expects($this->any())
      ->method('reportByUrl')
      ->will($this->returnValueMap([
        [
          'https://example.com',
          new DubBotEmbedJsonResponse(200, $this->randomMachineName(), 0),
        ],
        [
          'https://example-with-issues.com',
          new DubBotEmbedJsonResponse(200, $this->randomMachineName(), 10),
        ],
        [
          'https://invalid-example.com',
          new DubBotEmbedJsonResponse(404),
        ],
      ]));

    $this->configFactory = $this->getConfigFactoryStub([
      'dubbot.settings' => [
        'embed_key' => $this->randomMachineName(),
        'dialog_renderer' => 'off_canvas',
      ],
    ]);

    $this->linkGenerator = new LinkGenerator($this->requestStack, $this->dubbotClient, $this->configFactory);
    $this->linkGenerator->setStringTranslation($this->getStringTranslationStub());
    $this->linkGenerator->setUrl('https://example.com');
  }

  /**
   * Tests the link generation process.
   */
  public function testLink(): void {
    $link = $this->linkGenerator->generate();
    $attributes = $link->getUrl()->getOption('attributes');
    $this->assertContains('dubbot-enabled', $attributes['class']);
    $this->assertContains('dubbot-has-no-issues', $attributes['class']);
    $this->assertEquals('dubbot.report', $link->getUrl()->getRouteName());
    $this->assertEquals('DubBot (0)', $link->getText());

    $this->linkGenerator->setUrl('https://example-with-issues.com');
    $link = $this->linkGenerator->generate();
    $attributes = $link->getUrl()->getOption('attributes');
    $this->assertContains('dubbot-enabled', $attributes['class']);
    $this->assertContains('dubbot-has-issues', $attributes['class']);
    $this->assertEquals('dubbot.report', $link->getUrl()->getRouteName());
    $this->assertEquals('DubBot (10)', $link->getText());

    $this->linkGenerator->setUrl('https://invalid-example.com');
    $link = $this->linkGenerator->generate();
    $attributes = $link->getUrl()->getOption('attributes');
    $this->assertContains('dubbot-disabled', $attributes['class']);
    $this->assertEquals('<nolink>', $link->getUrl()->getRouteName());
    $this->assertEquals('DubBot (N/A)', $link->getText());
  }

  /**
   * Tests the link generation process.
   */
  public function testLinkCustomClass(): void {
    $link = $this->linkGenerator->generate(['my-class']);
    $attributes = $link->getUrl()->getOption('attributes');
    $this->assertContains('my-class', $attributes['class']);
  }

}
