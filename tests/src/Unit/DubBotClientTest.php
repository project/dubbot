<?php

namespace Drupal\Tests\dubbot\Unit;

use Drupal\dubbot\Client;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Prophecy\Argument;

/**
 * @covers \Drupal\dubbot\Client
 * @group dubbot
 */
class DubBotClientTest extends UnitTestCase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The HTTP client mock handler.
   *
   * @var \GuzzleHttp\Handler\MockHandler
   */
  protected $mockHandler;

  /**
   * The HTTP Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The DubBot client.
   *
   * @var \Drupal\dubbot\ClientInterface
   */
  protected $dubbotClient;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The DubBot domain negotiator.
   *
   * @var \Drupal\dubbot\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * The logger channel factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Random DubBot embed key for testing purposes.
   *
   * @var string
   */
  protected $embedKey;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->embedKey = $this->randomMachineName(16);
    $this->configFactory = $this->getConfigFactoryStub([
      'dubbot.settings' => [
        'embed_key' => $this->embedKey,
      ],
    ]);
    $this->mockHandler = new MockHandler();
    $handler = HandlerStack::create($this->mockHandler);
    $this->httpClient = new HttpClient(['handler' => $handler]);
    $this->cacheBackend = $this->createMock('Drupal\Core\Cache\CacheBackendInterface');
    $this->domainNegotiator = $this->createMock('Drupal\dubbot\DomainNegotiatorInterface');

    // Create a logger factory to produce the resulting logger.
    $logger = $this->prophesize('Drupal\Core\Logger\LoggerChannelInterface');
    $this->loggerFactory = $this->prophesize('Drupal\Core\Logger\LoggerChannelFactoryInterface');
    $this->loggerFactory->get(Argument::exact('dubbot'))->willReturn($logger->reveal());
    $this->loggerFactory = $this->loggerFactory->reveal();

    $this->dubbotClient = new Client($this->httpClient, $this->configFactory, $this->cacheBackend, $this->domainNegotiator, $this->loggerFactory);
  }

  /**
   * Tests the isEnabled() method.
   *
   * @dataProvider dataProviderTestIsEnabled
   */
  public function testIsEnabled($values, $responses, $expected): void {
    $this->cacheBackend->expects($this->exactly(count($values)))
      ->method('get')
      ->willReturn(...$values);
    $this->mockHandler->append(...$responses);

    $this->assertEquals($expected, $this->dubbotClient->isEnabled());
    $this->assertEquals($expected, $this->dubbotClient->isEnabled());
  }

  /**
   * Data provider for testIsEnabled().
   */
  public static function dataProviderTestIsEnabled(): array {
    return [
      [
        [FALSE],
        [new Response(401)],
        FALSE,
      ],
      [
        [FALSE],
        [new Response(200)],
        TRUE,
      ],
      [
        [(object) ['data' => FALSE]],
        [],
        FALSE,
      ],
      [
        [(object) ['data' => TRUE]],
        [],
        TRUE,
      ],
    ];
  }

  /**
   * Tests the isValidEmbedKey() method.
   */
  public function testIsValidEmbedKey(): void {
    $this->mockHandler->append(new Response(200), new Response(401), new Response(500));
    $this->assertTrue($this->dubbotClient->isValidEmbedKey($this->randomMachineName()));
    $this->assertFalse($this->dubbotClient->isValidEmbedKey($this->randomMachineName()));
    $this->assertFalse($this->dubbotClient->isValidEmbedKey($this->randomMachineName()));
  }

  /**
   * Tests the iframeReportUrlByUrl() method.
   */
  public function testIframeReportUrlByUrl(): void {
    $url = 'https://example.com';
    $encoded_url = str_replace('%2F', '/', rawurlencode($url));
    $expected = "https://api.dubbot.com/embeds/{$this->embedKey}?url=$encoded_url";
    $this->assertEquals($expected, $this->dubbotClient->iframeReportUrlByUrl($url));

    $this->configFactory = $this->getConfigFactoryStub([
      'dubbot.settings' => [
        'embed_key' => '',
      ],
    ]);
    $this->dubbotClient = new Client($this->httpClient, $this->configFactory, $this->cacheBackend, $this->domainNegotiator, $this->loggerFactory);
    $this->assertNull($this->dubbotClient->iframeReportUrlByUrl($url));
  }

  /**
   * Tests the iframeReportUrlByPageID() method.
   */
  public function testIframeReportUrlByPageId(): void {
    $page_id = $this->randomMachineName();
    $expected = "https://api.dubbot.com/embeds/{$this->embedKey}?page_id=$page_id";
    $this->assertEquals($expected, $this->dubbotClient->iframeReportUrlByPageId($page_id));

    $this->configFactory = $this->getConfigFactoryStub([
      'dubbot.settings' => [
        'embed_key' => '',
      ],
    ]);
    $this->dubbotClient = new Client($this->httpClient, $this->configFactory, $this->cacheBackend, $this->domainNegotiator, $this->loggerFactory);
    $this->assertNull($this->dubbotClient->iframeReportUrlByUrl($page_id));
  }

  /**
   * Tests the reportByUrl() method.
   */
  public function testReportByUrl(): void {
    $this->mockHandler->append(new Response(200), new Response(404), new RequestException('Error Communicating with Server', new Request('GET', 'test')));

    $this->assertTrue($this->dubbotClient->reportByUrl('http://example.com')->isValidResponse());
    $this->assertFalse($this->dubbotClient->reportByUrl('http://example.com')->isValidResponse());

    $this->expectException(RequestException::class);
    $this->dubbotClient->reportByUrl('http://example.com');
  }

  /**
   * Tests the reportByPageId() method.
   */
  public function testReportByPageId(): void {
    $this->mockHandler->append(new Response(200), new Response(404), new RequestException('Error Communicating with Server', new Request('GET', 'test')));

    $this->assertTrue($this->dubbotClient->reportByPageId($this->randomMachineName())->isValidResponse());
    $this->assertFalse($this->dubbotClient->reportByPageId($this->randomMachineName())->isValidResponse());

    $this->expectException(RequestException::class);
    $this->dubbotClient->reportByPageId($this->randomMachineName());
  }

}
