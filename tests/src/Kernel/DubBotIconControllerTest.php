<?php

namespace Drupal\Tests\dubbot\Kernel;

use Drupal\Core\Cache\Cache;
use Drupal\dubbot\Controller\DubBotIconController;
use Drupal\KernelTests\KernelTestBase;

/**
 * @covers \Drupal\dubbot\Controller\DubBotIconController
 * @group dubbot
 */
class DubBotIconControllerTest extends KernelTestBase {

  /**
   * The DubBot Icon controller.
   *
   * @var \Drupal\dubbot\Controller\DubBotIconController
   */
  protected $controller;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dubbot'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->controller = DubBotIconController::create($this->container);
  }

  /**
   * Tests the access callback.
   */
  public function testAccess(): void {
    $this->assertTrue($this->controller->access('ffffff')->isAllowed());
    $this->assertTrue($this->controller->access('070')->isAllowed());
    $this->assertFalse($this->controller->access('#ffffff')->isAllowed());
    $this->assertFalse($this->controller->access($this->randomMachineName())->isAllowed());
  }

  /**
   * Tests the build callback.
   */
  public function testbuild(): void {
    $result = $this->controller->build('FCB');

    $this->assertEquals(200, $result->getStatusCode());
    $this->assertEquals(Cache::PERMANENT, $result->getCacheableMetadata()->getCacheMaxAge());
    $re = '/fill="#FCB"/m';
    $matches = [];
    preg_match($re, $result->getContent(), $matches);
    $this->assertNotEmpty($matches);
  }

}
