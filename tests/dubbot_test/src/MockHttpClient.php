<?php

namespace Drupal\dubbot_test;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * Test HTTP client for the air quality test.
 */
class MockHttpClient extends Client {

  const MOCK_HTTP_CLIENT_RESPONSE = 'dubbot_test_http_client_response';

  /**
   * {@inheritdoc}
   */
  public function get($uri, array $options = []): ResponseInterface {
    $state = \Drupal::service('state');
    $return = $state->get(static::MOCK_HTTP_CLIENT_RESPONSE, new Response(200));

    if ($return instanceof \Exception) {
      throw $return;
    }

    if (is_callable($return)) {
      return call_user_func_array($return, [$options]);
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function post($uri, array $options = []): ResponseInterface {
    $state = \Drupal::service('state');
    $return = $state->get(static::MOCK_HTTP_CLIENT_RESPONSE, new Response(200));

    if ($return instanceof \Exception) {
      throw $return;
    }

    if (is_callable($return)) {
      return call_user_func_array($return, [$options]);
    }

    return $return;
  }

  /**
   * Builds a mock response for the DubBot Overview page.
   *
   * @param array $options
   *   The options sent to the HTTP Client request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The mocked endpoint response.
   */
  public static function buildOverviewResponse(array $options): ResponseInterface {
    $total = 51;
    extract($options['json']);
    $start = $page * $limit;

    $pages = [];

    $site_id = 'site_id';
    $i = 0;
    while ($i++ < $total) {
      $item = new \stdClass();
      $item->site_id = $site_id;
      $item->id = sprintf('%02d', $i);
      $item->total_issues_count = ($i % 2 == 0) ? $total + 1 - $i : $i;
      $item->path = 'https://example.com/page/' . $item->id;
      $item->title = 'Page ' . $item->id;
      $date = new \DateTime('2020-01-01');
      $interval = ($i % 2 == 1) ? $total + 1 - $i : $i;
      $item->crawled_at = $date->add(\DateInterval::createFromDateString("$interval minute"))->format('Y-m-d\TH:i:s.u\Z');
      $pages[] = $item;
    }

    usort($pages, function ($a, $b) use ($sort, $order) {
      $multiplier = ($order == 'desc') ? -1 : 1;

      if (is_numeric($a->{$sort})) {
        return $multiplier * ($a->{$sort} - $b->{$sort});
      }
      return $multiplier * strcmp($a->{$sort}, $b->{$sort});
    });

    $pages = array_slice($pages, $start, $limit);

    $result = [
      'pages' => $pages,
      'pagination' => [
        'current_page' => $page,
        'total_pages' => ceil($total / $limit),
        'total_items' => $total,
      ],
    ];

    return new Response(200, [], Json::encode((object) $result));
  }

  /**
   * Builds a mock response for the DubBot Overview page.
   *
   * @param array $options
   *   The options sent to the HTTP Client request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The mocked endpoint response.
   */
  public static function buildReportResponse(array $options): ResponseInterface {
    $crawled_at = new \Datetime('2023-05-05');
    $valid_body = [
      'page_id' => 'random_page_id',
      'total_issues_count' => rand(0, 25),
      'crawled_at' => $crawled_at->format('Y-m-d\TH:i:s.u\Z'),
    ];
    return new Response(200, [], Json::encode((object) $valid_body));
  }

}
